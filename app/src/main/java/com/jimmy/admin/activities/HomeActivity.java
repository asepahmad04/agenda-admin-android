package com.jimmy.admin.activities;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.Toast;

import com.jimmy.admin.R;
import com.jimmy.admin.fragments.ChatFragment;
import com.jimmy.admin.fragments.DaftarAgendaFragment;
import com.jimmy.admin.fragments.TambahAgendaFragment;
import com.jimmy.admin.helers.PermissionsManager;

import java.util.List;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PermissionsManager.PermissionsListener {

    private PermissionsManager permissionsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        checkPermission();

    }

    private void checkPermission() {
        if (!PermissionsManager.areLocationPermissionsGranted(this)) {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }else {
            open(ChatFragment.newInstance("admin-0"), "Daftar Agenda");
        }
    }

    public void open(Fragment f, String title) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, f).commit();
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Anda yakin ingin keluar dari aplikasi?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", (dialog, id) ->finish())
                    .setNegativeButton("Tidak", (dialog, id) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            open(TambahAgendaFragment.newInstance(), "Tambah Agenda");
        } else if (id == R.id.nav_list_agenda) {
            open(DaftarAgendaFragment.newInstance(), "Daftar Agenda");
        }else if(id == R.id.nav_list_chat){
            open(ChatFragment.newInstance("admin-0"), "Daftar Agenda");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if(!granted){
            Toast.makeText(this, "Lokasi harus diaktifkan", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            open(DaftarAgendaFragment.newInstance(), "Daftar Agenda");
        }
    }

}
