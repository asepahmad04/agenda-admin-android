package com.jimmy.admin.activities;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jimmy.admin.R;
import com.jimmy.admin.adapters.AgendaAdapter;
import com.jimmy.admin.adapters.ChatAdapter;
import com.jimmy.admin.helers.MyDate;
import com.jimmy.admin.helers.Util;
import com.jimmy.admin.models.ChatModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.listChat)
    RecyclerView listChat;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    @BindView(R.id.et_message)
    EditText edMsg;
    @BindView(R.id.icSend)
    ImageView icSend;
    private DatabaseReference roomRef;
    private ChatAdapter chatAdapter;
    private Query mRef;
    private String idRoom = "admin-0";

    public static void to(Context _context, String roomID, String user){
        Intent i = new Intent(_context, ChatActivity.class);
        i.putExtra("roomID", roomID);
        i.putExtra("user", user);
        _context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("roomID") && getIntent().hasExtra("user")) {
            idRoom = getIntent().getStringExtra("roomID");
            String mUser = getIntent().getStringExtra("user");
            getSupportActionBar().setTitle(mUser);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        chatAdapter = new ChatAdapter(this);
        listChat.setLayoutManager(new LinearLayoutManager(this));
        listChat.setAdapter(chatAdapter);

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("chat");
        roomRef = databaseReference.child(idRoom);
        mRef = roomRef.orderByKey().limitToLast(50);
        mRef.keepSynced(true);
        mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChatModel messages = dataSnapshot.getValue(ChatModel.class);
                chatAdapter.add(messages);
                chatAdapter.notifyDataSetChanged();
                listChat.scrollToPosition(chatAdapter.getItemCount() - 1);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        isTyping(false);
        edMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty() && s.length() > 0 && !s.equals("")) {
                    isTyping(true);
                } else {
                    isTyping(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        icSend.setOnClickListener(v -> {
            if (!edMsg.getText().toString().trim().isEmpty()) addMessage();
        });

    }

    private void addMessage() {
        ChatModel mData = new ChatModel();
        mData.setName("admin");
        mData.setMessage(edMsg.getText().toString());
        mData.setTime(MyDate.getCurDateTime());
        mData.setStatus("admin");
        roomRef.push().setValue(mData);
        edMsg.setText("");
        Util.hideSoftKeyboard(this);
    }

    private void isTyping(boolean typing) {
        icSend.setColorFilter(typing ? ContextCompat.getColor(this, R.color.colorPrimary) : ContextCompat.getColor(this, R.color.colorDarkGray), android.graphics.PorterDuff.Mode.MULTIPLY);
        icSend.setEnabled(typing);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
