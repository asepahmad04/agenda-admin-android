package com.jimmy.admin.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.jimmy.admin.activities.ChatActivity;
import com.jimmy.admin.adapters.AgendaAdapter;
import com.jimmy.admin.R;
import com.jimmy.admin.activities.HomeActivity;
import com.jimmy.admin.adapters.ChatAdapter;
import com.jimmy.admin.adapters.ListUserAdapter;
import com.jimmy.admin.helers.MyDate;
import com.jimmy.admin.models.ChatModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatFragment extends Fragment {

    @BindView(R.id.listUser)
    RecyclerView listUser;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    private ListUserAdapter adapter;

    public static ChatFragment newInstance(String roomID) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString("roomID", roomID);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);
        ButterKnife.bind(this, view);
        adapter = new ListUserAdapter(getContext());
        listUser.setLayoutManager(new LinearLayoutManager(getContext()));
        listUser.setAdapter(adapter);
        adapter.setOnItemClickListener((user, room, position) -> ChatActivity.to(getContext(), room, user));

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("chat");
        databaseReference.keepSynced(true);
        databaseReference.orderByKey().addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String room = dataSnapshot.getKey();
                adapter.add(room);
                adapter.notifyDataSetChanged();
                listUser.scrollToPosition(adapter.getItemCount() - 1);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;
    }

}
