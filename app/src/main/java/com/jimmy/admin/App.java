package com.jimmy.admin;

import androidx.multidex.MultiDexApplication;

import com.mapbox.mapboxsdk.Mapbox;

public class App extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Mapbox.getInstance(this, getResources().getString(R.string.token_mapbox));
    }
}
